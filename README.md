# polysun-demo-plugin

Polysun Demo Plugin
===========

This is a simple Polysun plugin for demonstration purposes.

The following controllers are in this plugin:

- DemoFlowratePluginController

Controller plugins are automatically detected by Polysun, see ControllerPlugin interface.

This code is Open Source, see [LICENSE.txt](https://bitbucket.org/velasolaris/polysun-demo-plugin/src/master/LICENSE.txt). It can be used to create new plugin controllers for use in Polysun.
If you would like to request your plugin to be included in Polysun, [create a fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) of the project [polysun-public-plugin](https://bitbucket.org/velasolaris/polysun-public-plugin/) (Bitbucket account required) and create a [pull request](https://www.atlassian.com/git/tutorials/making-a-pull-request). To develop external plugins, please create a [fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) of this project or create your own project from scratch.

Requirements for controller plugin development
----------------------------------------------

- [JDK 17](https://bell-sw.com/pages/downloads/#/java-17-current)
- [Gradle Build Tool](https://gradle.org/)

Build JAR
---------

- Linux/Mac: ./`gradlew fatJar`
- Windows: `gradlew.bat fatJar`

Development
-----------

To import the project into Eclipse, use File > Import... > Gradle > Existing Gradle Project.

Please refer to the following API documentations:

- [This project's Javadoc](https://javadoc.io/doc/com.velasolaris.polysun/polysun-demo-plugin)
- [Polysun Plugin Interface API](https://javadoc.io/doc/com.velasolaris.polysun/polysun-plugin-if)

Classes to get started with:

- IPluginController: Interface for plugin controllers
- AbstractPluginController: Abstract class for plugin controllers that implements the IPluginController interface
- ControllerPlugin: Interface for plugin detection
- AbstractControllerPlugin: Abstract class with the default implementations for detecting plugins.
