package com.velasolaris.plugin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.velasolaris.plugin.controller.demo.DemoFlowratePluginController;
import com.velasolaris.plugin.controller.spi.AbstractControllerPlugin;
import com.velasolaris.plugin.controller.spi.IPluginController;

/**
 * Demo Controller plugin for Polysun.
 *
 * @author rkurmann
 * @since Polysun 9.1
 *
 */
public class DemoControllerPlugin extends AbstractControllerPlugin {

    @Override
    public List<Class<? extends IPluginController>> getControllers(Map<String, Object> aParameters) {
        List<Class<? extends IPluginController>> controllers = new ArrayList<>();
        controllers.add(DemoFlowratePluginController.class);
        return controllers;
    }

    @Override
    public String getDescription() {
        return "Demo plugin controllers for Polysun";
    }

    @Override
    public String getCreator() {
        return "Vela Solaris AG";
    };

    @Override
    public String getVersion() {
        return "9.1";
    }


}
