import java.text.SimpleDateFormat
import java.util.*

plugins {
    eclipse
    idea
    `java-library`
    `maven-publish`
    signing
    id("com.palantir.git-version") version "0.12.3"
}

description = "Polysun flow rate plugin for demo purposes"
group = "com.velasolaris.polysun"
val gitVersion: groovy.lang.Closure<String> by extra
version = gitVersion()
        .replace(".dirty", "")
        .replace("-", ".")
        .replaceAfter("SNAPSHOT", "")
val isReleaseVersion = !version.toString().endsWith("SNAPSHOT")

repositories {
    mavenCentral()
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
        vendor.set(JvmVendorSpec.BELLSOFT)
    }
    withJavadocJar()
    withSourcesJar()
}

val pluginIfVersion = "1.1.0"
dependencies {
    api("com.velasolaris.polysun:polysun-plugin-if:$pluginIfVersion")
    implementation("commons-io:commons-io:2.11.0")
    testImplementation("junit:junit:4.13.2")
}

val javaVersion = System.getProperty("java.version")
val javaVendor = System.getProperty("java.vendor")
val javaVmVersion = System.getProperty("java.vm.version")
val osName = System.getProperty("os.name")
val osArchitecture = System.getProperty("os.arch")
val osVersion = System.getProperty("os.version")

tasks {
    javadoc {
        title = "Polysun demo plugin"
        val standardJavadocDocletOptions = options as StandardJavadocDocletOptions
        standardJavadocDocletOptions.addBooleanOption("html5", true)
        standardJavadocDocletOptions.stylesheetFile = project.file("javadoc_stylesheet.css")
        standardJavadocDocletOptions.links?.add("https://javadoc.io/doc/com.velasolaris.polysun/polysun-plugin-if/$pluginIfVersion")
        standardJavadocDocletOptions.links?.add("https://docs.oracle.com/en/java/javase/17/docs/api/")
        standardJavadocDocletOptions.addStringOption("Xdoclint:none", "-quiet")
    }
    jar {
        manifest {
            attributes["Library"] = rootProject.name
            attributes["Version"] = archiveVersion
            attributes["Company"] = "Vela Solaris AG"
            attributes["Website"] = "www.velasolaris.com"
            attributes["Built-By"] = System.getProperty("user.name")
            attributes["Build-Timestamp"] = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(Date())
            attributes["Created-by"] = "Gradle ${gradle.gradleVersion}"
            attributes["Build-OS"] = "$osName $osArchitecture $osVersion"
            attributes["Build-Jdk"] = "$javaVersion ($javaVendor $javaVmVersion)"
            attributes["Build-OS"] = "$osName $osArchitecture $osVersion"
        }
    }
    register("buildPlugin", Jar::class) {
        dependsOn(jar)
        from(configurations.runtimeClasspath.get()
        .onEach { println("add from dependencies: ${it.name}") }
        .map { if (it.isDirectory) it else zipTree(it) })
        archiveFileName.set("DemoPlugin.jar")
        duplicatesStrategy = DuplicatesStrategy.INCLUDE
        manifest {
            attributes["Library"] = rootProject.name
            attributes["Version"] = archiveVersion
            attributes["Company"] = "Vela Solaris AG"
            attributes["Website"] = "www.velasolaris.com"
            attributes["Built-By"] = System.getProperty("user.name")
            attributes["Build-Timestamp"] = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").format(Date())
            attributes["Created-by"] = "Gradle ${gradle.gradleVersion}"
            attributes["Build-OS"] = "$osName $osArchitecture $osVersion"
            attributes["Build-Jdk"] = "$javaVersion ($javaVendor $javaVmVersion)"
            attributes["Build-OS"] = "$osName $osArchitecture $osVersion"
        }
        with(jar.get() as CopySpec)
    }
}


configurePublication(rootProject)

fun configurePublication(project: Project) {
    publishing {
        publications {
            create<MavenPublication>(project.name) {
                groupId = group.toString()
                artifactId = project.name
                version = version
                from(project.components["java"])
                versionMapping {
                    usage("java-api") {
                        fromResolutionOf("runtimeClasspath")
                    }
                    usage("java-runtime") {
                        fromResolutionResult()
                    }
                }
                pom {
                    name.set(project.name)
                    description.set(project.description)
                    url.set("https://bitbucket.org/velasolaris/polysun-plugin-if")
                    developers {
                        developer {
                            id.set("rkurmann")
                            name.set("Roland Kurmann")
                        }
                        developer {
                            id.set("mjakobi")
                            name.set("Marc Jakobi")
                        }
                    }
                    issueManagement {
                        system.set("Bitbucket")
                        url.set("https://bitbucket.org/velasolaris/polysun-demo-plugin/issues")
                    }
                    scm {
                        url.set("https://bitbucket.org/velasolaris/polysun-demo-plugin")
                        connection.set("scm:git:git://bitbucket.org/velasolaris/polysun-demo-plugin.git")
                        developerConnection.set("scm:git:ssh://git@bitbucket.org:velasolaris/polysun-demo-plugin.git")
                    }
                    licenses {
                        license {
                            name.set("MIT license")
                            url.set("https://bitbucket.org/velasolaris/polysun-demo-plugin/src/master/LICENSE.txt")
                            distribution.set("repo")
                        }
                    }
                }
            }
            repositories {
                maven {
                    val releasesRepoUrl = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
                    val snapshotsRepoUrl = uri("https://oss.sonatype.org/content/repositories/snapshots/")
                    url = if (isReleaseVersion) releasesRepoUrl else snapshotsRepoUrl
                    credentials {
                        username = project.properties["ossrhUser"]?.toString() ?: "Unknown user"
                        password = project.properties["ossrhPassword"]?.toString() ?: "Unknown password"
                    }
                }
            }
        }
    }
    signing {
        if (isReleaseVersion) {
            sign(publishing.publications[project.name])
        }
    }
}

